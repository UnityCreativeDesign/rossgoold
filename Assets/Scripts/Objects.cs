﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
    public class Objects : MonoBehaviour
    {

        void Awake()
        {
            ReadObjects();
        }

        public void ReadObjects()
        {
        // due to the limitations of array's in Json, I had to wrap the data inside another class to the loop through and extract it
            string json = File.ReadAllText(Application.streamingAssetsPath + "/Objects.json");
            JsonData data = JsonUtility.FromJson<JsonData>(json);

            for (int i = 0; i < data.spawnObjects.Length; i++)
            {
                SpawnObject obj = data.spawnObjects[i];
                
                // Debug.Log(obj.primitive);

                // Creating the game game object - using enum as PrimitiveType cannot be deseralized 
                GameObject go = GameObject.CreatePrimitive((PrimitiveType)System.Enum.Parse(typeof(PrimitiveType), obj.primitive));
                Information info = go.AddComponent<Information>() as Information;
                info.Initialise(obj.information);

                // Correcting the postion, rotation and scale of the 
                go.transform.position = obj.position;
                go.transform.rotation = Quaternion.Euler(obj.rotation);
                go.transform.localScale = obj.scale;

                // Not necessary but sets the parent of the object to the empty game object for neatness
                go.transform.SetParent(transform);
            }
        }

    // Class that will store an array of the data
        [System.Serializable]
        private class JsonData
        {
            public SpawnObject[] spawnObjects;
        }

    // Class that holds the Json Objects
        [System.Serializable]
        private class SpawnObject
        {
            public string name;
            public Vector3 position;
            public Vector3 rotation;
            public Vector3 scale;
            public string primitive;
            public SpawnObjectInfo information;
        }

    // Class that holds the Information class within the SpawnObject
        [System.Serializable]
        public class SpawnObjectInfo
        {
            public string stringField;
            public int intField;
            public double floatField;
            public bool boolField;
        }
    }
