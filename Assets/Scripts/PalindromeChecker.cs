﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalindromeChecker : MonoBehaviour
{
    public string stringToCheck;

    public void CheckString(string str)
    {
        //char[] charactersToTrim = { ' ' };

        //string origToTrim = str.Trim(charactersToTrim);
        string origToTrim = str.Replace(" ", "");
        char[] originalStringArrayTrimmed = origToTrim.ToCharArray();
        char[] trimmedArrayToBeReversed = origToTrim.ToCharArray();

        Array.Reverse(trimmedArrayToBeReversed);

        string originalTrimmed = new string(originalStringArrayTrimmed);
        string reversedTrimmed = new string(trimmedArrayToBeReversed);

        bool equal = originalTrimmed.Equals(reversedTrimmed, StringComparison.InvariantCultureIgnoreCase);

        if (equal)
        {
            Debug.Log("The input string is a Palindrome.");
            Debug.Log("The string: " + str + " and it's reverse are equal." );
        }
        else if (!equal)
        {
            Debug.Log("The input string is NOT a Palindrome.");
            Debug.Log("The string: " + str + " and it's reverse are NOT equal.");
        }
    }
}
