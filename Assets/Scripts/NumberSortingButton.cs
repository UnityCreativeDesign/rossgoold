﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

// This script creates a few buttons on the editor for the Number Sorting Task

[CustomEditor(typeof(NumberSorting))]
public class NumberSortingButton : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        NumberSorting numberSortingScript = (NumberSorting)target;
        if(GUILayout.Button("Arrange Numbers"))
        {
            numberSortingScript.ArrangeNumbers();
            Debug.Log("------ End of method output ------");
        }
        else if (GUILayout.Button("Print Multiples"))
        {
            numberSortingScript.PrintMultiples(numberSortingScript.numberToCheck);
            Debug.Log("------ End of method output ------");
        }
        else if (GUILayout.Button("Print Prime Numbers"))
        {
            numberSortingScript.PrintPrimeNumbers();
            Debug.Log("------ End of method output ------");
        }
    }
}
