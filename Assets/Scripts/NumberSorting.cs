﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;

public class NumberSorting : MonoBehaviour
{
    // For TASK 3
    public int numberToCheck = 1;

    // List of numbers 
    private List<int> numbersList = new List<int>();

    #region TASK 1 - Read list of numbers from the text file
    void Start()
    {
        try
        {
            // Creating an instance of stream reader called reader to read from the Numbers.txt 
            using (StreamReader reader = new StreamReader("Assets/StreamingAssets/Numbers.txt"))
            {
                // temporary holder for the line
                string tempLine;

                // reads each line until the end of the file
                while ((tempLine = reader.ReadLine()) != null)
                {
                    // creates temporary integer, changes the string to an int, then adds the int to the list
                    int temp = int.Parse(tempLine);
                    numbersList.Add(temp);
                }
            }
        }
        catch(Exception e)
        {
            Debug.Log("File could not be read:");
            Debug.LogException(e, this);
        }
    }
    #endregion

    #region TASK 2 - Arrange all numbers from lowest to highest
    public void ArrangeNumbers()
    {
        // Not sure if this is efficient way to do it, but works for this task
        numbersList.Sort();
        foreach (int number in numbersList)
        {
            Debug.Log(number);
        }
    }
    #endregion

    #region TASK 3 - Given integer, print all multiples of the integer in the list
    // (multiples not duplicates?)
    public void PrintMultiples(int num)
    {
        // for each loops takes each integer in the list and checks if the number given is a multiple
        foreach (int number in numbersList)
        {
            // if number in the list divided by the given number is a multiple - whereby number / num has no remainder
            if((number % num) == 0)
            {
                Debug.Log("The number: " + number + " is a multiple of: " + num);
            }
        }        
    }
    #endregion

    #region TASK 4 - Print any prime number in the list to console
    public void PrintPrimeNumbers()
    {
        // prints all numbers in the list that are prime
        Debug.Log("Printing Primes");

        foreach (int number in numbersList)
        {
            int a = 0;
            for (int i = 1; i <= number; i++)
            {
                if (number % i == 0)
                {
                    a++;
                }
            }
            if(a == 2)
            {
                Debug.Log(number + " is a prime number");
            }
        }
    }
    #endregion
}
