﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class Information : MonoBehaviour
    {
    // variables
        public string stringField;
        public int intField;
        public float floatField;
        public bool boolField;

    // method that can be called to create an instance of the class
        public void Initialise(Objects.SpawnObjectInfo inf)
        {
            stringField = inf.stringField;
            intField = inf.intField;
            floatField = (float)inf.floatField;
            boolField = inf.boolField;
        }

    }
