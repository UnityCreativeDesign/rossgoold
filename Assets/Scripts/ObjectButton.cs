﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Objects))]
public class ObjectButton : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Objects spawnPrimativesScript = (Objects)target;
        if (GUILayout.Button("Spawn Primatives"))
        {
            spawnPrimativesScript.ReadObjects();
            Debug.Log("------ End of method output ------");
        }
    }
}
