﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

// This script creates a button on the editor for the Plaindrome Task

[CustomEditor(typeof(PalindromeChecker))]
public class PalindromeButton : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        PalindromeChecker stringCheckerScript = (PalindromeChecker)target;
        if (GUILayout.Button("Check String"))
        {
            stringCheckerScript.CheckString(stringCheckerScript.stringToCheck);
            Debug.Log("------ End of method output ------");
        }       
    }
}
